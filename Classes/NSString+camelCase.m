//
//  NSString+camelCase.m
//  imMeta2
//
//  Created by Simon Germain on 2/21/12.
//  Copyright (c) 2012 Turner Broadcasting System, Inc. All rights reserved.
//

#import "NSString+camelCase.h"

@implementation NSString (camelCase)

- (NSString *)toCamelCase {
	return [NSString stringWithFormat:@"%@%@", [[NSString stringWithFormat:@"%c", [self characterAtIndex:0]] lowercaseString], [self substringFromIndex:1]];
}

- (NSString *)capitalize {
    
    return [NSString stringWithFormat:@"%@%@", [[NSString stringWithFormat:@"%c", [self characterAtIndex:0]] uppercaseString], [self substringFromIndex:1]];
}

@end
