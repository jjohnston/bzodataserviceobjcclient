//
//  NSString+camelCase.h
//  imMeta2
//
//  Created by Simon Germain on 2/21/12.
//  Copyright (c) 2012 Turner Broadcasting System, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (camelCase)

- (NSString *)toCamelCase;
- (NSString *)capitalize;

@end
